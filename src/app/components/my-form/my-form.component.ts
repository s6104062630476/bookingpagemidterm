import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-my-form',
  templateUrl: './my-form.component.html',
  styleUrls: ['./my-form.component.css']
})
export class MyFormComponent implements OnInit {
  form: FormGroup;
  typeR: string = "One-way";
  show: boolean = false;

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      fullname: ['', Validators.required],
      from: ['', Validators.required],
      to: ['', Validators.required],
      departure: ['', Validators.required],
      arrival: ['-', Validators.required],
      type: [''],
      adults: ['0'],
      children: ['0'],
      infants: ['0']
    })
  }

  ngOnInit(): void {
  }

  radioChangeHandler (event: any) {
    this.typeR = event.target.value
  }

  submitForm(): void {
    this.show = true
  }
}
