import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MyFormComponent } from './components/my-form/my-form.component';
import { GreetComponent } from './components/greet/greet.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FriendComponent } from './components/friend/friend.component'

@NgModule({
  declarations: [
    AppComponent,
    MyFormComponent,
    GreetComponent,
    FriendComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
